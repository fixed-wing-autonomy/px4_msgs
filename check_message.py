import os
import sys

if __name__ == '__main__':
    uorb_message_name = sys.argv[1]
    pieces = uorb_message_name.split('_')
    capital_pieces = [p.capitalize() for p in pieces]
    ros_message_name = ''.join(capital_pieces)

    os.system('diff ~/recuv/px4/msg/{}.msg msg/{}.msg'.format(
        uorb_message_name, ros_message_name))
    copy_stuff = input('\nupdate message? [y/N]')
    if copy_stuff == 'y':
        os.system('cp ~/recuv/px4/msg/{}.msg msg/{}.msg'.format(
            uorb_message_name, ros_message_name))
